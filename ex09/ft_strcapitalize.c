/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/04 20:51:02 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/06 12:13:30 by rblondia         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

int	is_alpha(char c)
{
	if ((c >= 'a' && c <= 'z')
		|| (c >= 'A' && c <= 'Z')
		|| (c >= '0' && c <= '9'))
		return (1);
	return (0);
}

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (is_alpha(str[i]) == 1
			&& (str[i] >= 'A' && str[i] <= 'Z'))
			str[i] = str[i] + 32;
		if (is_alpha(str[i - 1]) == 0
			&& is_alpha(str[i]) == 1
			&& !(str[i] >= '0' && str[i] <= '9'))
			str[i] = str[i] - 32;
		i++;
	}
	return (str);
}
