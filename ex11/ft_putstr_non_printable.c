/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/06 15:37:39 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/20 16:13:52 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int	ft_is_printable(char a)
{
	if (a >= 32 && a <= 126)
		return (1);
	return (0);
}

void	ft_putstr_non_printable(char *str)
{
	int		i;
	char	*base;

	base = "0123456789abcdef";
	i = 0;
	while (str[i])
	{
		if (ft_is_printable(str[i]) == 0)
		{
			write(1, "\\", 1);
			write(1, &base[str[i] / 16], 1);
			write(1, &base[str[i] % 16], 1);
		}
		else
			write(1, &str[i], 1);
		i++;
	}
}
